const getData = require("../data/getData");
const writer = require("./writer");

const matches = getData().getMatches();
const deliveries = getData().getDeliveries();

function top10EconomicalBowlers2015() {
  try {
    let matchesIn2015 = {};

    for (let index = 0; index < matches.length; index++) {
      if (matches[index].season === "2015") {
        matchesIn2015[matches[index].id] = "";
      }
    }

    let bowlerStats = {};

    for (let index = 0; index < deliveries.length; index++) {
      if (deliveries[index].match_id in matchesIn2015) {
        let isValidBall = deliveries[index].noball_runs == "0" && deliveries[index].wide_runs == "0";

        let total_runs =  parseInt(deliveries[index].total_runs);

        if (!isValidBall) {
          total_runs += 1;
        }

        if (bowlerStats[deliveries[index].bowler] === undefined) {
          bowlerStats[deliveries[index].bowler] = {
            runs: total_runs,
            balls: isValidBall,
          };
        } else {
          bowlerStats[deliveries[index].bowler] = {
            runs: bowlerStats[deliveries[index].bowler].runs + total_runs,
            balls: bowlerStats[deliveries[index].bowler].balls + isValidBall,
          };
        }
      }
    }

    let economy = {};

    for (const key in bowlerStats) {
      let overs = bowlerStats[key].balls / 6;
      let currentEconomy = bowlerStats[key].runs / overs;
      economy[key] = currentEconomy;
    }

    let economyEntries = Object.entries(economy);

    for (let row_index = 0; row_index < economyEntries.length - 1; row_index++) {
      for (let index = 0; index < economyEntries.length - row_index - 1; index++) {
        if (economyEntries[index][1] > economyEntries[index + 1][1]) {
          let temp = economyEntries[index];
          economyEntries[index] = economyEntries[index + 1];
          economyEntries[index + 1] = temp;
        }
      }
    }

    let top10EconomicalBowlers = {};
    let index = 0;

    for (const pair of entries) {
      if (index <= 9) {
        const [key, value] = pair;
        top10EconomicalBowlers[key] = value;
        index++;
      }
    }

    const jsonData = JSON.stringify(top10EconomicalBowlers);

    return jsonData;
  } catch (error) {
    console.log(error);
  }
}

const result = top10EconomicalBowlers2015();

writer("top10EconomicalBowlers2015.json", result);
