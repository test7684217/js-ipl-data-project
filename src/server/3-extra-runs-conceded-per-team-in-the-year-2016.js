const getData = require("../data/getData");
const writer = require("./writer");

const matches = getData().getMatches();
const deliveries = getData().getDeliveries();

function extraRunsConceded() {

  try {

    let matchesIn2016 = {};

    for (let index = 0; index < matches.length; index++) {
      if (matches[index].season === "2016") {
        matchesIn2016[matches[index].id] = "";
      }
    }

    let concededRunsObject = {};

    for (let index = 0; index < deliveries.length; index++) {
      if (deliveries[index].match_id in matchesIn2016) {
        if (concededRunsObject[deliveries[index].bowling_team] === undefined) {
          concededRunsObject[deliveries[index].bowling_team] = parseInt(
            deliveries[index].extra_runs
          );
        } else {
          concededRunsObject[deliveries[index].bowling_team] += parseInt(
            deliveries[index].extra_runs
          );
        }
      }
    }

    let jsonData = JSON.stringify(concededRunsObject);

    return jsonData;

  } catch (error) {
    console.log(error);
  }
}

const result = extraRunsConceded();

writer("extraRunsConcededPerTeam2016.json",result)

