const getData = require("../data/getData");
const writer = require("./writer");

const deliveries = getData().getDeliveries();

function highestDimissals() {
  try {
    let ans = {};

    let max = 0;
    let pair = "";

    for (let index = 0; index < deliveries.length; index++) {
      if (deliveries[index].player_dismissed) {
        let key =
          deliveries[index].player_dismissed + "#" + deliveries[index].bowler;
        if (ans[key] == undefined) ans[key] = 0;

        ans[key] += 1;

        if (max < ans[key]) {
          max = ans[key];
          pair = key;
        }
      }
    }
    
    const highestNumberOfDismissals = {
      "dimissed player": pair.split("#")[0],
      "dismissed by": pair.split("#")[1],
      "dismissals": max
    };


    let jsonData = JSON.stringify(highestNumberOfDismissals);

    return jsonData;
  } catch (error) {
    console.log(error);
  }
}

const result = highestDimissals();

writer("highestDismissals.json", result);
