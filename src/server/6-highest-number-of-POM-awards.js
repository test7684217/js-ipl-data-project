const getData = require("../data/getData");
const writer  = require("./writer");

const matches = getData().getMatches();

function highestNumberOfPlayerOfMatchAwards() {
  try {
    let players = {};

    let highestNumberofPlayerOfTheMatchAwardsPerSeason = {};

    for (let index = 0; index < matches.length; index++) {
      if (players[matches[index].season] == undefined) {
        players[matches[index].season] = {};
      }

      if (
        players[matches[index].season][matches[index].player_of_match] ==
        undefined
      ) {
        players[matches[index].season][matches[index].player_of_match] = 1;
      } else {
        players[matches[index].season][matches[index].player_of_match] += 1;
      }
    }

    for (const key in players) {
      let obj = players[key];
      let max = 0;
      let player_name = null;

      for (const player in obj) {
        if (max < obj[player]) {
          max = obj[player];
          player_name = player;
        }
      }

      highestNumberofPlayerOfTheMatchAwardsPerSeason[key] = player_name;
    }

    const jsonData = JSON.stringify(highestNumberofPlayerOfTheMatchAwardsPerSeason);

    return jsonData;
  } catch (error) {
    console.log(error);
  }
}


const result = highestNumberOfPlayerOfMatchAwards();

writer("highestNumberOfPOMAwardsPlayer.json",result)
