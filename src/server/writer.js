const fs = require("fs");
const path = require("path");

function writer(fileName, jsonData) {
  try {
    let filePath = path.join(__dirname, "..", "public", "output", fileName);

    fs.writeFile(
      filePath,
      jsonData,
      { encoding: "utf8", flag: "w", mode: 0o666 },
      (err) => {
        console.log(err);
      }
    );

    console.log(`Data saved in ${fileName} successfully`);
  } catch (error) {
    console.log(`Error in saving data to ${fileName}`);
    console.log(error);
  }
}

module.exports = writer;
