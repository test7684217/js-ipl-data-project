const getData = require("../data/getData");
const writer = require("./writer");

const matches = getData().getMatches();

function numberOfMatchesWonOfAllTeams(data) {
  try {
    let result = {};

    for (let index = 0; index < matches.length; index++) {
      if (result[matches[index].winner] == undefined) {
        result[matches[index].winner] = 1;
      } else {
        result[matches[index].winner] += 1;
      }
    }

    let jsonData = JSON.stringify(result)
    return jsonData;

  } catch (error) {
    console.log(error);
  }
}

const result = numberOfMatchesWonOfAllTeams(matches);

writer("numberOfMatchesWonOfAllTeams.json",result)
