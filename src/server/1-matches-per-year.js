const getData = require("../data/getData");
const writer = require("./writer");

const matches = getData().getMatches();

function matchesPerYear(data) {
  try {
    let matchesPerYearObj = {};

    for (let index = 0; index < data.length; index++) {
      if (matchesPerYearObj[data[index].season] === undefined) {
        matchesPerYearObj[data[index].season] = 1;
      } else {
        matchesPerYearObj[data[index].season] += 1;
      }
    }

    let jsonData = JSON.stringify(matchesPerYearObj);
    
    return jsonData;

  } catch (error) {
    console.log("Error while finding matches played per year");
    console.log(error);
  }
}

const result = matchesPerYear(matches);

writer("matchesPerYear.json", result);
