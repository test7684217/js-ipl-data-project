const getData = require("../data/getData");
const writer = require("./writer");

const matches = getData().getMatches();

function matchesWonPerTeamPerYear(data) {
  try {
    let matchesWonPerTeamPerYearData = {};

    for (let index = 0; index < data.length; index++) {
      if (matchesWonPerTeamPerYearData[data[index].season] == undefined) {
        matchesWonPerTeamPerYearData[data[index].season] = {};
      }

      if (
        matchesWonPerTeamPerYearData[data[index].season][data[index].winner] ==
        undefined
      ) {
        matchesWonPerTeamPerYearData[data[index].season][
          data[index].winner
        ] = 1;
      } else {
        matchesWonPerTeamPerYearData[data[index].season][
          data[index].winner
        ] += 1;
      }
    }

    let jsonData = JSON.stringify(matchesWonPerTeamPerYearData);

    return jsonData;
  } catch (error) {
    console.log(error);
  }
}

const result = matchesWonPerTeamPerYear(matches);

writer("matchesPerYearPerTeam.json", result);
