const getData = require("../data/getData");
const writer = require("./writer");

const deliveries = getData().getDeliveries();

function bestEconomySuperOvers() {
  try {
    let bowlers = {};

    for (let index = 0; index < deliveries.length; index++) {
      if (deliveries[index].is_super_over !== "0") {

        let isValidBall = deliveries[index].noball_runs == "0" && deliveries[index].wide_runs == "0";

        let total_runs =  parseInt(deliveries[index].total_runs);

        if (!isValidBall) {
          total_runs += 1;
        }

        if (bowlers[deliveries[index].bowler] === undefined) {
          bowlers[deliveries[index].bowler] = {
            runs: total_runs,
            balls: isValidBall,
          };
        } else {
          bowlers[deliveries[index].bowler] = {
            runs: bowlers[deliveries[index].bowler].runs + total_runs,
            balls: bowlers[deliveries[index].bowler].balls + isValidBall,
          };
        }
      }
    }

    let best_economy = null;
    let best_economy_bowler;

    for (let key in bowlers) {
      let overs = bowlers[key].balls / 6;
      let curr_economy = bowlers[key].runs / overs;

      if (best_economy === null) {
        best_economy = curr_economy;
      }
      if (curr_economy < best_economy) {
        best_economy = curr_economy;
        best_economy_bowler = key;
      }
    }

    let bestEconomyBowlerInSuperOvers = {
      best_economy_bowler,
      best_economy,
    };

    let jsonData = JSON.stringify(bestEconomyBowlerInSuperOvers);

    return jsonData;
  } catch (error) {
    console.log(error);
  }
}

const result = bestEconomySuperOvers();

writer("bestEconomySuperOver.json", result);
