const getData = require("../data/getData");
const writer = require("./writer");

const matches = getData().getMatches();

function frequencyOfTossAndWins() {
  try {
    let teamsWon = {};

    for (let index = 0; index < matches.length; index++) {
      if (matches[index].toss_winner === matches[index].winner) {
        if (teamsWon[matches[index].toss_winner] == undefined) {
          teamsWon[matches[index].toss_winner] = 1;
        } else {
          teamsWon[matches[index].toss_winner] += 1;
        }
      }
    }

    const jsonData = JSON.stringify(teamsWon);

    return jsonData;
  } catch (error) {
    console.log(error);
  }
}

const result = frequencyOfTossAndWins();

writer("numberOfTimesTeamWonMatchAndToss.json", result);
