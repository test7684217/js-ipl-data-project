const getData = require("../data/getData");
const writer = require("./writer");

const deliveries = getData().getDeliveries();
const matches = getData().getMatches();


function strikeRate() {
  try {
    let players = {};

    for (let index = 0; index < matches.length; index++) {
      let obj = {};

      if (players[matches[index].season] === undefined) {
        players[matches[index].season] = {};
      } else {
        obj = players[matches[index].season];
      }

      let id = matches[index].id;

      for (let delivery of deliveries) {
        if (delivery.match_id === id) {
          let isValidBall =
            delivery.noball_runs === "0" &&
            delivery.wide_runs === "0";

          if (obj[delivery.batsman] === undefined) {
            obj[delivery.batsman] = {
              runs: parseInt(delivery.batsman_runs),
              balls: isValidBall ? 1 : 0,
            };
          } else {
            obj[delivery.batsman].runs +=
              parseInt(delivery.batsman_runs) +
              (isValidBall ? 1 : 0);

            obj[delivery.batsman].balls += isValidBall ? 1 : 0;
          }
        }
      }

      players[matches[index].season] = obj;
    }

    let strikeRatesOfBatsmenPerSeason = {};

    for (let year in players) {
      let obj = {};

      for (let item in players[year]) {
        let strikeRate = 0;
        if (players[year][item].balls > 0) {
          strikeRate = (players[year][item].runs / players[year][item].balls) * 100;
        }
        obj[item] = strikeRate.toFixed(2);
      }

      strikeRatesOfBatsmenPerSeason[year] = obj;
    }

    const jsonData = JSON.stringify(strikeRatesOfBatsmenPerSeason);

    return jsonData;
  } catch (error) {
    console.log(error);
  }
}


const result = strikeRate();

writer("strikeRateOfbatsmanPerYear.json", result);
